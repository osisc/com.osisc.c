# OSISC C

## Open Source Integrated System Components in C

* No C++
* No IDL Compiler
* No Threads
* No Make
* No Cygwin, MingGW, MSYS, ...

Clean and beautiful object oriented code for high performance, responsive, binary compatible systems on any OS or hardware platform.
