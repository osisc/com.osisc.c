/*
 *  Copyright (c) 2018 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define FILE StringFormat
#define PACKAGE com_osisc_c

INCLUDE("com/osisc/c/Object.h")
INCLUDE("com/osisc/c/target/Type.h")

IMPORT("com/osisc/c/Object.h")
IMPORT("com/osisc/c/target/Type.h")

#define AS ENUM
#define NAME IOKind
BEGIN
    EXTENDS(SignedByte)
    VAL(VERBATIM, = 0)          /* Format string */
    VAL(PERCENT,)
    VAL(STRING,)                /* Argument string */
    VAL(CHAR,)
    VAL(INT,)
    VAL(POINTER,)
    VAL(FLOAT,)
    VAL(NUM_CHARS,)             /* Standard "%n" for number of chars printed */
    VAL(VALUES,)
END
#undef NAME
#undef AS

STRUCT(TypeData,
    VAR(Byte, size,)
    VAR(Byte, align,)
)

#define AS ENUM
#define NAME TypeID
BEGIN
    EXTENDS(SignedByte)
    VAL(VOID, = 0)
    VAL(CHAR,)
    VAL(SHORT,)
    VAL(INT,)
    VAL(LONG,)
    VAL(LONG_LONG,)
    VAL(SIZE,)
    VAL(PTRDIFF,)
    VAL(WCHAR,)
    VAL(POINTER,)
    VAL(FLOAT,)
    VAL(DOUBLE,)
    VAL(LONG_DOUBLE,)
    VAL(VALUES,)
END
#undef NAME
#undef AS

#define AS CLASS
#define NAME Types
BEGIN
    FIXED(TypeData, data, [TypeID_VALUES])
    INLINE(Byte, size, (TypeID id), { 
        return Fixed_byte(&Types_class.data[id].size); 
    })
    INLINE(Byte, align, (TypeID id), {
        return Fixed_byte(&Types_class.data[id].align); 
    })
END
#undef NAME
#undef AS

#define AS CLASS
#define NAME StringFormat
BEGIN
    EXTENDS(Object)
    VAR(fixed char *, next,)

    VAR(IOKind, kind,)
    VAR(TypeID, type,)
    VAR(Byte, radix,)            /* 0 for 10 on output, but prefix on input */
    VAR(UnsignedShort, width,)
    VAR(UnsignedShort, precision,)
    
    VAR(unsigned, skip, :1)
    VAR(unsigned, isWide, :1)
    VAR(unsigned, isSigned, :1)
    VAR(unsigned, zeroPad, :1)
    VAR(unsigned, leftAlign, :1)
    VAR(unsigned, plusSign, :1)
    VAR(unsigned, spaceSign, :1)
    VAR(unsigned, upperCase, :1)
    VAR(unsigned, altForm, :1)
    VAR(unsigned, fixForm, :1) 
    VAR(unsigned, expForm, :1)
    
    VAR(unsigned, widthArg, :1)
    VAR(unsigned, precisionArg, :1)
             
    CONSTRUCTOR(StringFormat *, set, (StringFormat * this, fixed char * s))
END
#undef NAME
#undef AS
