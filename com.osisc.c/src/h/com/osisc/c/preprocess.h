/*
 *  Copyright (c) 2020 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define VOID com_osisc_c_VOID
#define COMMA com_osisc_c_COMMA
#define CONDITION com_osisc_c_CONDITION
#define CONDITION_1 com_osisc_c_CONDITION_1
#define CONDITION_0 com_osisc_c_CONDITION_0
#define STRING com_osisc_c_STRING
#define STRINGIFY com_osisc_c_STRINGIFY
#define TOKEN com_osisc_c_TOKEN
#define TOKENIZE com_osisc_c_TOKENIZE

#ifndef com_osisc_c_defines_declarations
#define com_osisc_c_defines_declarations

#define com_osisc_c_VOID
#define com_osisc_c_COMMA ,

#define com_osisc_c_STRING(_A_) # _A_
#define com_osisc_c_STRINGIFY(_A_) com_osisc_c_STRING(_A_)

#define com_osisc_c_TOKEN(_A_,_B_) _A_ ## _B_
#define com_osisc_c_TOKENIZE(_A_,_B_) com_osisc_c_TOKEN(_A_,_B_)

#define com_osisc_c_CONDITION_1(_1_, _0_) _1_
#define com_osisc_c_CONDITION_0(_1_, _0_) _0_

#define com_osisc_c_CONDITION(_PREDICATE_) \
    com_osisc_c_TOKENIZE(com_osisc_c_CONDITION_,_PREDICATE_)

#endif

#ifndef IMPORTS

#undef VOID
#undef COMMA
#undef CONDITION
#undef CONDITION_1
#undef CONDITION_0
#undef STRING
#undef STRINGIFY
#undef TOKEN
#undef TOKENIZE

#endif
