/*
 *  Copyright (c) 2018-2020 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define _CLASSIFY(_AS_, _TYPE_) _AS_##_##_TYPE_
#define CLASSIFY(_AS_, _TYPE_) _CLASSIFY(_AS_, _TYPE_)

#define INCLUDE(_NAME_)
#define IMPORT(_NAME_)
#define DEFINE(_NAME_, _TEXT_)
#define MACRO(_NAME_, _PARAMS_, _TEXT_)
#define TYPE(_TYPE_, _NAME_, _DIM_)
#define STRUCT(_NAME_, _MEMBERS_)
#define UNION(_NAME_, _MEMBERS_)

#define BEGIN CLASSIFY(AS, BEGIN)
#define INTERFACE_BEGIN
#define CLASS_BEGIN
#define ENUM_BEGIN

#define END CLASSIFY(AS, END)
#define INTERFACE_END
#define CLASS_END
#define ENUM_END

#define VAR CLASSIFY(AS, VAR)
#define CLASS_VAR(_TYPE_, _NAME_, _DIM_)
#define AS_VAR(_TYPE_, _NAME_, _DIM_)

#define VAL(_NAME_, _VALUE_)

#define FIXED CLASSIFY(AS, FIXED)
#define INTERFACE_FIXED(_TYPE_, _NAME_, _DIM_)
#define CLASS_FIXED(_TYPE_, _NAME_, _DIM_)

#define PHANTOM(_TYPE_, _NAME_, _DIM_)

#define ABSTRACT CLASSIFY(AS, ABSTRACT)
#define CLASS_ABSTRACT(_TYPE_, _NAME_, _PARAMS_)
#define INTERFACE_ABSTRACT(_TYPE_, _NAME_, _PARAMS_)

#define FEATURE(_NAME_, _DEFAULT_)
#define CONSTRUCTOR(_TYPE_, _NAME_, _PARAMS_)
#define FUNCTION(_TYPE_, _NAME_, _PARAMS_)
#define INLINE(_TYPE_, _NAME_, _PARAMS_, _BLOCK_)
#define METHOD(_TYPE_, _NAME_, _PARAMS_)

#define EXTENSIBLE

#define EXTENDS CLASSIFY(AS, EXTENDS)
#define CLASS_EXTENDS(_NAME_)
#define INTERFACE_EXTENDS(_NAME_)
#define ENUM_EXTENDS(_NAME_)

#define IMPLEMENTS(_NAME_)
#define REIMPLEMENTS(_NAME_)
