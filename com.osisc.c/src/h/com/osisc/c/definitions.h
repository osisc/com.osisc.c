/*
 *  Copyright (c) 2018-2020 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

/* Input **********************************************************************/

#ifndef DEFINITION
#   error DEFINITION must be defined and point to the .def file to process
#endif

/* Internals ******************************************************************/

#define __TOKEN_(_PRE_,_POST_) _PRE_##_POST_
#define _TOKEN_(_PRE_,_POST_) __TOKEN_(_PRE_,_POST_)
#define __DOT_(_PRE_,_POST_) _PRE_##_##_POST_
#define _DOT_(_PRE_,_POST_) __DOT_(_PRE_,_POST_)
#define __STR_(_TXT_) #_TXT_
#define _STR_(_TXT_) __STR_(_TXT_)

#define _H_ #

/* Include guard **************************************************************/

#include "com/osisc/c/def.h"

#include _STR_(DEFINITION)

_H_ ifndef _DOT_(_DOT_(PACKAGE, FILE),includes)
_H_ define _DOT_(_DOT_(PACKAGE, FILE),includes)

#include "com/osisc/c/undef.h"

/* Includes *******************************************************************/

#include "com/osisc/c/def.h"

#undef INCLUDE
#define INCLUDE(_NAME_) \
    _H_ include _NAME_

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

_H_ include "com/osisc/c/preprocess.h"
_H_ include "com/osisc/c/target/Type.h"

_H_ endif

/* Internal imports: types, object types, functions ***************************/

#include "com/osisc/c/def.h"

#undef DEFINE
#define DEFINE(_NAME_, _TEXT_) \
    _H_ define _NAME_ _DOT_(PACKAGE, _NAME_)
#undef MACRO
#define MACRO(_NAME_, PARAMS_, _TEXT_) \
    _H_ define _NAME_ _DOT_(PACKAGE, _NAME_)
#undef TYPE
#define TYPE(_TYPE_, _NAME_, _DIM_) \
    _H_ define _NAME_ _DOT_(PACKAGE, _NAME_)
#undef STRUCT
#define STRUCT(_NAME_, _MEMBERS_) \
    _H_ define _NAME_ _DOT_(PACKAGE, _NAME_)
#undef UNION
#define UNION(_NAME_, _MEMBERS_) \
    _H_ define _NAME_ _DOT_(PACKAGE, _NAME_)
#undef ENUM_BEGIN
#define ENUM_BEGIN \
    _H_ define NAME _DOT_(PACKAGE, NAME)
#undef ENUM_END
#define ENUM_END \
    _H_ define _TOKEN_(NAME, Value) _DOT_(PACKAGE, _TOKEN_(NAME,Value))
#undef INTERFACE_BEGIN
#define INTERFACE_BEGIN \
    _H_ define NAME _DOT_(PACKAGE, NAME)
#undef CLASS_BEGIN
#define CLASS_BEGIN \
    _H_ define NAME _DOT_(PACKAGE, NAME)
#undef CONSTRUCTOR
#define CONSTRUCTOR(_TYPE_, _NAME_, _PARAMS_) \
    _H_ define _DOT_(NAME, _NAME_) _DOT_(PACKAGE, _DOT_(NAME, _NAME_))
#undef FUNCTION
#define FUNCTION(_TYPE_, _NAME_, _PARAMS_) \
    _H_ define _DOT_(NAME, _NAME_) _DOT_(PACKAGE, _DOT_(NAME, _NAME_))
#undef PHANTOM
#define PHANTOM(_TYPE_, _NAME_, _DIM_) \
    _H_ define _DOT_(NAME, _NAME_) _DOT_(PACKAGE, _DOT_(NAME, _NAME_))
#undef INLINE
#define INLINE(_TYPE_, _NAME_, _PARAMS_, _BLOCK_) \
    _H_ define _DOT_(NAME, _NAME_) _DOT_(PACKAGE, _DOT_(NAME, _NAME_))
#undef METHOD
#define METHOD(_TYPE_, _NAME_, _PARAMS_) \
    _H_ define _DOT_(NAME, _NAME_) _DOT_(PACKAGE, _DOT_(NAME, _NAME_))
#undef VAL
#define VAL(_NAME_, _VALUE_) \
    _H_ define _DOT_(NAME, _NAME_) _DOT_(PACKAGE, _DOT_(NAME, _NAME_))

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Internal imports: class types **********************************************/

#include "com/osisc/c/def.h"

#undef INTERFACE_BEGIN
#define INTERFACE_BEGIN \
    _H_ define _TOKEN_(NAME, Class) _DOT_(PACKAGE, _TOKEN_(NAME, Class))
#undef CLASS_BEGIN
#define CLASS_BEGIN \
    _H_ define _TOKEN_(NAME, Class) _DOT_(PACKAGE, _TOKEN_(NAME, Class))

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Internal imports: class objects ********************************************/

#include "com/osisc/c/def.h"

#undef INTERFACE_BEGIN
#define INTERFACE_BEGIN \
    _H_ define _DOT_(NAME, class) _DOT_(PACKAGE, _DOT_(NAME, class))
#undef CLASS_BEGIN
#define CLASS_BEGIN \
    _H_ define _DOT_(NAME, class) _DOT_(PACKAGE, _DOT_(NAME, class))

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Internal imports: class interface objects **********************************/

#include "com/osisc/c/def.h"

#undef CLASS_BEGIN
#define CLASS_BEGIN \
    _H_ define _DOT_(NAME, interfaces) _DOT_(PACKAGE, _DOT_(NAME, interfaces))
#undef IMPLEMENTS
#define IMPLEMENTS(_NAME_) \
    _H_ define _DOT_(_DOT_(NAME, _NAME_), class) _DOT_(PACKAGE, _DOT_(_DOT_(NAME, _NAME_), class))
#undef REIMPLEMENTS
#define REIMPLEMENTS(_NAME_) \
    _H_ define _DOT_(_DOT_(NAME, _NAME_), class) _DOT_(PACKAGE, _DOT_(_DOT_(NAME, _NAME_), class))

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Internal imports: class interface enums ************************************/

#include "com/osisc/c/def.h"

#undef CLASS_BEGIN
#define CLASS_BEGIN \
    _H_ define _DOT_(NAME,Interfaces) _DOT_(PACKAGE,_DOT_(NAME,Interfaces) )
#undef CLASS_END
#define CLASS_END \
    _H_ define _DOT_(NAME,Interfaces_SIZE) _DOT_(PACKAGE,_DOT_(NAME,Interfaces_SIZE))
#undef CLASS_EXTENDS
#define CLASS_EXTENDS(_NAME_) \
    _H_ define _DOT_(NAME,Interfaces_BASE) _DOT_(PACKAGE,_DOT_(NAME,Interfaces_BASE))
#undef IMPLEMENTS
#define IMPLEMENTS(_NAME_) \
    _H_ define _DOT_(_DOT_(NAME,_NAME_),ID) _DOT_(PACKAGE,_DOT_(_DOT_(NAME,_NAME_),ID))

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Internal imports: class features *******************************************/

#include "com/osisc/c/def.h"

#undef FEATURE
#define FEATURE(_NAME_, _DEFAULT_) \
    _H_ define _DOT_(NAME,_NAME_) _DOT_(PACKAGE,_DOT_(NAME,_NAME_))
#undef IMPLEMENTS
#define IMPLEMENTS(_NAME_) \
    _H_ define _DOT_(NAME,_NAME_) _DOT_(PACKAGE,_DOT_(NAME,_NAME_))

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Definitions include guard **************************************************/

#include "com/osisc/c/def.h"

#include _STR_(DEFINITION)

_H_ ifndef _DOT_(_DOT_(PACKAGE, FILE),definitions)
_H_ define _DOT_(_DOT_(PACKAGE, FILE),definitions)

#include "com/osisc/c/undef.h"

/* External imports ***********************************************************/

#include "com/osisc/c/def.h"

#undef IMPORT
#define IMPORT(_NAME_) \
    _H_ include _NAME_

_H_ define IMPORTS
#include _STR_(DEFINITION)
_H_ undef IMPORTS

#include "com/osisc/c/undef.h"

/* Preprocessor definitions ***************************************************/

#include "com/osisc/c/def.h"

#undef DEFINE
#define DEFINE(_NAME_, _TEXT_) \
    _H_ define _DOT_(PACKAGE, _NAME_) _TEXT_
#undef MACRO
#define MACRO(_NAME_, _PARAMS_, _TEXT_) \
    _H_ define _DOT_(PACKAGE, _NAME_)_PARAMS_ _TEXT_
#undef FEATURE
#define FEATURE(_NAME_, _DEFAULT_) \
    _H_ define _DOT_(com_osisc_c_CONDITION,_DOT_(_DOT_(PACKAGE,NAME),_NAME_))(_1_,_0_) \
        com_osisc_c_CONDITION(_DEFAULT_)(_1_,_0_)
#undef IMPLEMENTS
#define IMPLEMENTS(_NAME_) \
    _H_ define _DOT_(com_osisc_c_CONDITION,_DOT_(_DOT_(PACKAGE,NAME),_NAME_))(_1_,_0_) _1_

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Typedefs *******************************************************************/

#include "com/osisc/c/def.h"

#undef TYPE
#define TYPE(_TYPE_, _NAME_, _DIM_) \
    typedef _TYPE_ _NAME_ _DIM_;
#undef STRUCT
#define STRUCT(_NAME_, _MEMBERS_) \
    typedef struct _NAME_ _NAME_;
#undef UNION
#define UNION(_NAME_, _MEMBERS_) \
    typedef union _NAME_ _NAME_;
#undef ENUM_BEGIN
#define ENUM_BEGIN \
    typedef enum _TOKEN_(NAME,Value) _TOKEN_(NAME,Value);
#undef ENUM_EXTENDS
#define ENUM_EXTENDS(_NAME_) \
    typedef _NAME_ NAME;
#undef INTERFACE_BEGIN
#define INTERFACE_BEGIN \
    typedef struct _TOKEN_(NAME,Class) _TOKEN_(NAME,Class); \
    typedef struct Object NAME;
#undef CLASS_BEGIN
#define CLASS_BEGIN \
    typedef struct _TOKEN_(NAME,Class) _TOKEN_(NAME,Class); \
    typedef struct NAME NAME;

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Struct, union, enum types **************************************************/

#include "com/osisc/c/def.h"

#undef STRUCT
#define STRUCT(_NAME_, _MEMBERS_) \
    struct _NAME_ { \
        _MEMBERS_ \
    };
#undef UNION
#define UNION(_NAME_, _MEMBERS_) \
    union _NAME_ { \
        _MEMBERS_ \
    };
#undef ENUM_BEGIN
#define ENUM_BEGIN \
    enum _TOKEN_(NAME,Value) {
#undef ENUM_END
#define ENUM_END \
    };
#undef VAL
#define VAL(_NAME_, _VALUE_) \
    _DOT_(NAME,_NAME_) _VALUE_,
#undef AS_VAR
#define AS_VAR(_TYPE_, _NAME_, _DIM_) \
    _TYPE_ _NAME_ _DIM_;

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Class struct types *********************************************************/

#include "com/osisc/c/def.h"

#undef INTERFACE_BEGIN
#define INTERFACE_BEGIN \
    struct _TOKEN_(NAME,Class) {
#undef INTERFACE_END
#define INTERFACE_END \
    };
#undef INTERFACE_FIXED
#define INTERFACE_FIXED(_TYPE_, _NAME_, _DIM_) \
    _TYPE_ _NAME_ _DIM_;
#undef INTERFACE_ABSTRACT
#define INTERFACE_ABSTRACT(_TYPE_, _NAME_, _PARAMS_) \
    _TYPE_ (*_NAME_) _PARAMS_;
#undef INTERFACE_EXTENDS
#define INTERFACE_EXTENDS(_NAME_) \
    _TOKEN_(_NAME_,Class) base;

#undef CLASS_BEGIN
#define CLASS_BEGIN \
    struct _TOKEN_(NAME,Class) {
#undef CLASS_END
#define CLASS_END \
    };
#undef CLASS_FIXED
#define CLASS_FIXED(_TYPE_, _NAME_, _DIM_) \
    _TYPE_ _NAME_ _DIM_;
#undef CLASS_ABSTRACT
#define CLASS_ABSTRACT(_TYPE_, _NAME_, _PARAMS_) \
    _TYPE_ (*_NAME_) _PARAMS_;
#undef METHOD
#define METHOD(_TYPE_, _NAME_, _PARAMS_) \
    _TYPE_ (*_NAME_) _PARAMS_;
#undef CLASS_EXTENDS
#define CLASS_EXTENDS(_NAME_) \
    _TOKEN_(_NAME_,Class) base;

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Interface list enums *******************************************************/

#include "com/osisc/c/def.h"

#undef CLASS_BEGIN
#define CLASS_BEGIN \
    enum _DOT_(NAME,Interfaces) {
#undef CLASS_END
#define CLASS_END \
        _DOT_(NAME,Interfaces_SIZE) \
    };
#undef CLASS_EXTENDS
#define CLASS_EXTENDS(_NAME_) \
    _DOT_(NAME,Interfaces_BASE) = _DOT_(_NAME_,Interfaces_SIZE) - 1,
#undef IMPLEMENTS
#define IMPLEMENTS(_NAME_) \
    com_osisc_c_CONDITION(_DOT_(NAME,_NAME_))(_DOT_(_DOT_(NAME,_NAME_),ID) com_osisc_c_COMMA,)

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Object struct types ********************************************************/

#include "com/osisc/c/def.h"

#undef CLASS_BEGIN
#define CLASS_BEGIN \
    struct NAME {
#undef CLASS_END
#define CLASS_END \
    };
#undef CLASS_VAR
#define CLASS_VAR(_TYPE_, _NAME_, _DIM_) \
    _TYPE_ _NAME_ _DIM_;
#undef CLASS_EXTENDS
#define CLASS_EXTENDS(_NAME_) \
    _NAME_ base;

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Functions and phantom declarations *****************************************/

#include "com/osisc/c/def.h"

#undef CONSTRUCTOR
#define CONSTRUCTOR(_TYPE_, _NAME_, _PARAMS_) \
    _TYPE_ _DOT_(NAME,_NAME_) _PARAMS_;
#undef FUNCTION
#define FUNCTION(_TYPE_, _NAME_, _PARAMS_) \
    _TYPE_ _DOT_(NAME,_NAME_) _PARAMS_;
#undef PHANTOM
#define PHANTOM(_TYPE_, _NAME_, _DIM_) \
    extern phantom _TYPE_ _DOT_(NAME,_NAME_) _DIM_ phantomattr;
#undef METHOD
#define METHOD(_TYPE_, _NAME_, _PARAMS_) \
    _TYPE_ _DOT_(NAME,_NAME_) _PARAMS_;

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Class, interface and inline declarations ***********************************/

#include "com/osisc/c/def.h"

#undef INTERFACE_BEGIN
#define INTERFACE_BEGIN \
    extern com_osisc_c_target_phantom com_osisc_c_InterfaceClass _DOT_(NAME,class) com_osisc_c_target_phantomattr;
#undef CLASS_BEGIN
#define CLASS_BEGIN \
    extern com_osisc_c_target_fixed _TOKEN_(NAME,Class) _DOT_(NAME,class) com_osisc_c_target_fixedattr; \
    extern com_osisc_c_target_fixed com_osisc_c_InterfaceClass * com_osisc_c_target_fixed _DOT_(NAME,interfaces)[] com_osisc_c_target_fixedattr;
#undef IMPLEMENTS
#define IMPLEMENTS(_NAME_) \
    extern com_osisc_c_target_fixed _TOKEN_(_NAME_,Class) _DOT_(_DOT_(NAME,_NAME_),class) com_osisc_c_target_fixedattr;
#undef REIMPLEMENTS
#define REIMPLEMENTS(_NAME_) \
    extern com_osisc_c_target_fixed _TOKEN_(_NAME_,Class) _DOT_(_DOT_(NAME,_NAME_),class) com_osisc_c_target_fixedattr;
#undef INLINE
#define INLINE(_TYPE_, _NAME_, _PARAMS_, _BLOCK_) \
    static com_osisc_c_target_inlined _TYPE_ _DOT_(NAME,_NAME_) _PARAMS_ com_osisc_c_target_inlinedattr; \
    static com_osisc_c_target_inlined _TYPE_ _DOT_(NAME,_NAME_) _PARAMS_ _BLOCK_

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* External unimports *********************************************************/

#include "com/osisc/c/def.h"

#undef IMPORT
#define IMPORT(_NAME_) \
    _H_ include _NAME_

_H_ define UNIMPORTS
#include _STR_(DEFINITION)
_H_ undef UNIMPORTS

_H_ endif

#include "com/osisc/c/undef.h"

/* Internal unimports: types, object types, functions *************************/

#include "com/osisc/c/def.h"

#undef DEFINE
#define DEFINE(_NAME_, _TEXT_) \
    _H_ undef _NAME_
#undef MACRO
#define MACRO(_NAME_, _PARAMS_ ,_TEXT_) \
    _H_ undef _NAME_
#undef TYPE
#define TYPE(_TYPE_, _NAME_, _DIM_) \
    _H_ undef _NAME_
#undef STRUCT
#define STRUCT(_NAME_, _MEMBERS_) \
    _H_ undef _NAME_
#undef UNION
#define UNION(_NAME_, _MEMBERS_) \
    _H_ undef _NAME_
#undef ENUM_BEGIN
#define ENUM_BEGIN \
    _H_ undef NAME
#undef INTERFACE_BEGIN
#define INTERFACE_BEGIN \
    _H_ undef NAME
#undef CLASS_BEGIN
#define CLASS_BEGIN \
    _H_ undef NAME
#undef CONSTRUCTOR
#define CONSTRUCTOR(_TYPE_, _NAME_, _PARAMS_) \
    _H_ undef _DOT_(NAME, _NAME_)
#undef FUNCTION
#define FUNCTION(_TYPE_, _NAME_, _PARAMS_) \
    _H_ undef _DOT_(NAME, _NAME_)
#undef PHANTOM
#define PHANTOM(_TYPE_, _NAME_, _DIM_) \
    _H_ undef _DOT_(NAME, _NAME_)
#undef INLINE
#define INLINE(_TYPE_, _NAME_, _PARAMS_, _BLOCK_) \
    _H_ undef _DOT_(NAME, _NAME_)
#undef METHOD
#define METHOD(_TYPE_, _NAME_, _PARAMS_) \
    _H_ undef _DOT_(NAME, _NAME_)
#undef VAL
#define VAL(_NAME_, _VALUE_) \
    _H_ undef _DOT_(NAME, _NAME_)

_H_ ifndef IMPORTS

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Internal unimports: class types ********************************************/

#include "com/osisc/c/def.h"

#undef INTERFACE_BEGIN
#define INTERFACE_BEGIN \
    _H_ undef _TOKEN_(NAME, Class)

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Internal unimports: class objects ******************************************/

#include "com/osisc/c/def.h"

#undef INTERFACE_BEGIN
#define INTERFACE_BEGIN \
    _H_ undef _DOT_(NAME, class)
#undef CLASS_BEGIN
#define CLASS_BEGIN \
    _H_ undef _DOT_(NAME, class)

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Internal unimports: class interface objects ********************************/

#include "com/osisc/c/def.h"

#undef CLASS_BEGIN
#define CLASS_BEGIN \
    _H_ undef _DOT_(NAME, interfaces)
#undef IMPLEMENTS
#define IMPLEMENTS(_NAME_) \
    _H_ undef _DOT_(_DOT_(NAME, _NAME_), class)
#undef REIMPLEMENTS
#define REIMPLEMENTS(_NAME_) \
    _H_ undef _DOT_(_DOT_(NAME, _NAME_), class)

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Internal unimports: class interface enums **********************************/

#include "com/osisc/c/def.h"

#undef CLASS_BEGIN
#define CLASS_BEGIN \
    _H_ undef _DOT_(NAME,Interfaces)
#undef CLASS_END
#define CLASS_END \
    _H_ undef _DOT_(NAME,Interfaces_SIZE)
#undef CLASS_EXTENDS
#define CLASS_EXTENDS(_NAME_) \
    _H_ undef _DOT_(NAME,Interfaces_BASE)
#undef IMPLEMENTS
#define IMPLEMENTS(_NAME_) \
    _H_ undef _DOT_(_DOT_(NAME,_NAME_),ID)

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

/* Internal unimports: class features *****************************************/

#include "com/osisc/c/def.h"

#undef FEATURE
#define FEATURE(_NAME_, _DEFAULT_) \
    _H_ undef _DOT_(NAME,_NAME_)
#undef IMPLEMENTS
#define IMPLEMENTS(_NAME_) \
    _H_ undef _DOT_(NAME,_NAME_)

#include _STR_(DEFINITION)

#include "com/osisc/c/undef.h"

_H_ endif

/* Undefine internals *********************************************************/

#undef _H_

#undef __TOKEN_
#undef _TOKEN_
#undef __DOT_
#undef _DOT_
#undef _STRING
#undef STRING
