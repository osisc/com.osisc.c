/*
 *  Copyright (c) 2018-2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */


#undef CLASSIFY
#undef _CLASSIFY

#undef FILE
#undef PACKAGE

#undef INCLUDE
#undef IMPORT
#undef DEFINE
#undef MACRO
#undef TYPE
#undef STRUCT
#undef UNION

#undef BEGIN
#undef CLASS_BEGIN
#undef INTERFACE_BEGIN
#undef ENUM_BEGIN

#undef END
#undef CLASS_END
#undef INTERFACE_END
#undef ENUM_END

#undef VAR
#undef CLASS_VAR
#undef AS_VAR

#undef VAL

#undef FIXED
#undef CLASS_FIXED
#undef INTERFACE_FIXED

#undef PHANTOM

#undef ABSTRACT
#undef CLASS_ABSTRACT
#undef INTERFACE_ABSTRACT

#undef FEATURE
#undef CONSTRUCTOR
#undef FUNCTION
#undef INLINE
#undef METHOD

#undef EXTENSIBLE

#undef EXTENDS
#undef CLASS_EXTENDS
#undef INTERFACE_EXTENDS
#undef ENUM_EXTENDS

#undef IMPLEMENTS
#undef REIMPLEMENTS
