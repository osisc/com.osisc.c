/*
 *  Copyright (c) 2018 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_Log

#include <string.h>

#include "com/osisc/c/Log.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/Location.h"
#include "com/osisc/c/target/OutputFile.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Log.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/Location.h"
#include "com/osisc/c/target/OutputFile.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

typedef struct LogEntry LogEntry;

struct LogEntry {
    Size size;         /* Must be first. Accessed without regard for overflow */
    UnsignedShort cycle;
    Location location;
    Type data[1];
};

typedef struct LogPrivates LogPrivates;

struct LogPrivates {
    Log public;
    Type * first;
    Type * next;
    Type * endOfEntries;
    LogEntry entries[1];
};

Bool Log_valid(const Log * this, Bool throw) {
    (void)this;
    (void)throw;
    /* FIXME: Implement! */
    return 0;
}

static Log * Log_set(LogPrivates * private, Size size, OutputFile * out) {
    int valid = Log_valid(&private->public, 0);
    
    LogEntry * firstEntry = private->entries;
    Size logSize = size - offsetof(LogPrivates, entries);
    Type * endOfEntries = (Type *)((Byte *)private->entries + logSize);

    if (!valid || private->endOfEntries != endOfEntries) {
        ((Object *)private)->class = &Log_class.base;

        private->first = (Type *)firstEntry;
        private->next = (Type *)firstEntry;
        private->endOfEntries = endOfEntries;
        firstEntry->size = logSize;
        firstEntry->cycle = 0;
        Location_set(&firstEntry->location, &Log_class, &Package_class);
    }

    private->public.out = out;
    return &private->public;
}

Log * Log_create(fixed LogConfigurationClass * config, OutputFile * out) {
    void * address = Fixed_pointer(&config->address);
    Size size = Fixed_size(&config->size);
    return Log_set(address, size, out);
}

static Size Log_sizeofArray(void * start, void * end) {
    if (start < end) {
        return (Size)((Byte *)end - (Byte *)start);
    }
    return SIZE(0);
}

static Size Log_entrySize(LogPrivates * private, Size entrySize) {
    Size logSize = Log_sizeofArray(private->entries, private->endOfEntries);

    entrySize -= sizeof(Object);
    entrySize += offsetof(LogEntry, data);

    if (entrySize > logSize) {
        entrySize = logSize;
    }
    return entrySize;
}

typedef struct LogEntrySizes LogEntrySizes;

struct LogEntrySizes {
    Size available;
    Size overflow;
};

static LogEntrySizes
Log_entrySizes(LogPrivates * private, void * entry, Size size) {
    LogEntrySizes sizes = { Log_sizeofArray(entry, private->endOfEntries), 0 };

    if (size <= sizes.available) {
        sizes.available = size;
    } else {
        sizes.overflow = size - sizes.available;
    }
    return sizes;
}

static Type *
Log_nextEntry(LogPrivates * private, void * entry, LogEntrySizes sizes) {
    void * next = (sizes.overflow == 0)
        ? (Byte *)entry + sizes.available
        : (Byte *)private->entries + sizes.overflow;
    return next;
}

static void *
Log_nextField(LogPrivates * private, void * entry, Size offset, LogEntrySizes sizes) {
    void * field = (offset < sizes.available)
        ? (Byte *)entry + offset
        : (Byte *)private->entries + offset - sizes.available;
    return field;
}

static void
Log_setData(LogPrivates * private, void * field, void * data, Size size) {
    LogEntrySizes sizes = Log_entrySizes(private, field, size);

    memcpy(field, data, sizes.available);
    if (sizes.overflow > 0) {
        memcpy(private->entries, (Byte *)data + sizes.available, sizes.overflow);
    }
}

static void
Log_setLocation(LogPrivates * private, void * field, fixed ObjectClass * class) {
    Location location;
    Log_setData(private, field, Location_set0(&location, class), sizeof(Location));
}

typedef InterfaceClass * InstanceOfFunction(const Object * this, fixed InterfaceClass * i);

void Log_exception(Log * this, Exception * e) {
    LogPrivates * private = (LogPrivates *)this;

    fixed ObjectClass * class = ((Object *)e)->class;
    fixed InterfaceClass * sc = Object_interfaceOf((Object *)e, Exception_Sized_ID, &Sized_class);
    Size entrySize = Log_entrySize(private, ((SizedClass *)sc)->size);

    Type * entry = private->next;
    LogEntrySizes entrySizes = Log_entrySizes(private, entry, entrySize);
    Type * first = private->first;
    LogEntrySizes firstSizes = Log_entrySizes(private, first, first->asSize);

    /* Move first if we are about to overwrite */
    if (first > entry) {
        Type * endOfEntry = (Type *)((Byte *)entry + entrySizes.available);
        while (first < endOfEntry) {
            firstSizes = Log_entrySizes(private, first, first->asSize);
            first = Log_nextEntry(private, first, firstSizes);
            if (firstSizes.overflow > 0) {
                break;
            }
        }
    }
    if (entrySizes.overflow > 0) {
        Type * endOfEntry = (Type *)((Byte *)private->entries + entrySizes.overflow);
        while (first < endOfEntry) {
            firstSizes = Log_entrySizes(private, first, first->asSize);
            first = Log_nextEntry(private, first, firstSizes);
        }
    }

    /* Set new log state */
    private->first = first;
    private->next = Log_nextEntry(private, entry, entrySizes);

    /* Set new log entry state */
    entry->asSize = entrySize;
    {
        void * data;
        Type * field;

        field = Log_nextField(private, entry, offsetof(LogEntry, cycle), entrySizes);
        field->asUnsignedShort = Core_cycle();
        field = Log_nextField(private, entry, offsetof(LogEntry, location), entrySizes);
        Log_setLocation(private, field, class);
        field = Log_nextField(private, entry, offsetof(LogEntry, data), entrySizes);
        data = (Byte *)e + sizeof(Object);
        Log_setData(private, field, data, entrySize - offsetof(LogEntry, data));
    }
}

void Log_save(Log * this) {
    /* TODO: Implement! */
    (void)this;
}

fixed InterfaceClass * fixed Log_interfaces[] fixedattr = {
    (void *) &Log_interfaces[Log_Interfaces_SIZE]
    Interface_LIST(Log_Assertable, &Log_Assertable_class)
};

fixed struct LogClass Log_class fixedattr = {
    {
        &Package_class,
        Log_interfaces
    }
};
