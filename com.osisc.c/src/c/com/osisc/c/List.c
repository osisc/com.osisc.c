/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_List

#include "com/osisc/c/Object.h"
#include "com/osisc/c/List.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/List.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

List * List_set(List * this) {
    Object_set(&this->base, &List_class.base);
    this->first = (void *)0;
    this->last = (void *)0;
    return this;
}

void List_addFirst(List * this, ListLink * item) {
    ListLink * first = this->first;

    item->next = first;

    if (!first) {
        this->last = item;
    }
    this->first = item;
}

void List_addLast(List * this, ListLink * item) {
    ListLink * last = this->last;

	item->next = (void *)0;
	
    if (last) {
        last->next = item;
    } else {
        this->first = item;
    }
    this->last = item;
}

ListLink * List_removeFirst(List * this) {
    ListLink * first = this->first;

    if (first) {
        this->first = first->next;
        first->next = (void *)0;
    }
    return first;
}

fixed InterfaceClass * fixed List_interfaces[] fixedattr = {
    (void *) &List_interfaces[List_Interfaces_SIZE]
    Interface_LIST(List_Assertable, &List_Assertable_class)
};

fixed struct ListClass List_class fixedattr = {
    {
        &Package_class,
        List_interfaces
    }
};

