/*
 *  Copyright (c) 2018-2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_SignedAssertion

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

phantom char SignedAssertion_headFormat[] phantomattr =
    EXCEPTION_H_FMT "\t" "Expected" "\t" "Actual";
phantom char SignedAssertion_dataFormat[] phantomattr =
    EXCEPTION_D_FMT "\t" "%" LONG_D_FMT "\t" "%" LONG_D_FMT;

fixed PrintableClass SignedAssertion_Printable_class fixedattr = {
    { &Printable_class },
    SignedAssertion_headFormat,
    SignedAssertion_dataFormat
};
