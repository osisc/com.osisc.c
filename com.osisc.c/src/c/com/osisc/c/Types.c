/*
 *  Copyright (c) 2018 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_Types

#include <stddef.h>

#include "com/osisc/c/StringFormat.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/StringFormat.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

fixed struct TypesClass Types_class fixedattr = {
    {
        { 0, 1 },
        { sizeof(char), alignof(char) },
        { sizeof(short), alignof(short) },
        { sizeof(int), alignof(int) },
        { sizeof(long), alignof(long) },
        { sizeof(long long), alignof(long long) },
        { sizeof(size_t), alignof(size_t) },
        { sizeof(ptrdiff_t), alignof(ptrdiff_t) },
        { sizeof(wchar_t), alignof(wchar_t) },
        { sizeof(void *), alignof(void *) },
        { sizeof(float), alignof(float) },
        { sizeof(double), alignof(double) },
        { sizeof(long double), alignof(long double) }
    }
};
