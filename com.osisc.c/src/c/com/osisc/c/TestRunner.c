/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_TestRunner

#include "com/osisc/c/Test.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Test.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

TestRunner *
TestRunner_set(TestRunner * this, OutputFile * out) {
    Object_set((Object *)this, (fixed ObjectClass *)&TestRunner_class);
    this->out = out;
    this->total = 0;
    this->success = 0;
    return this;
}

void TestRunner_run(TestRunner * this, Test * test) {
    static fixed char dot[] fixedattr = ".";
    TestClass * class = (TestClass *)((Object *)test)->class;
    TestFunction * fixed * functions = Fixed_fixpointer(&class->functions);
    TestFunction * function;

    while ((function = Fixed_pointer(functions++))) {
        this->total++;
        function(test);
        Core_runJobs();
        this->success++;

        if (this->out) {
            OutputFile_printf(this->out, dot);
        }
    }
}

void TestRunner_done(TestRunner * this) {
    static fixed char newline[] fixedattr = "\n";

    if (this->out) {
        OutputFile_printf(this->out, newline);
        OutputFile_oprinth(this->out, (Object *)this);
        OutputFile_oprint(this->out, (Object *)this);
    }
}

fixed InterfaceClass * fixed TestRunner_interfaces[] fixedattr = {
    (void *) &TestRunner_interfaces[TestRunner_Interfaces_SIZE]
    Interface_LIST(TestRunner_Sized, &TestRunner_Sized_class)
    Interface_LIST(TestRunner_Printable, &TestRunner_Printable_class)
};

fixed TestRunnerClass TestRunner_class fixedattr = {
    {
        &Package_class,
        TestRunner_interfaces
    }
};

