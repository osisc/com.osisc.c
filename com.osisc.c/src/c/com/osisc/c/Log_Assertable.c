/*
 *  Copyright (c) 2018-2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_Log

#include <string.h>

#include "com/osisc/c/Log.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Log.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

void Log_assert(const Object * this) {
    Log_valid((Log *)this, Bool_TRUE);
}

fixed AssertableClass Log_Assertable_class fixedattr = {
    { &Assertable_class },
    Log_assert
};
