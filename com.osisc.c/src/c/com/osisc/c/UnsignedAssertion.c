/*
 *  Copyright (c) 2018-2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_UnsignedAssertion

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

UnsignedAssertion *
UnsignedAssertion_set1(UnsignedAssertion * this, fixed UnsignedAssertionClass * class,
                      fixed void * pc, void * sp, fixed PackageClass * package,
                      const UnsignedLong expected, const UnsignedLong actual) {
    Assertion_set1(&this->base, &class->base, pc, sp, package);
    this->expected = expected;
    this->actual = actual;
    return this;
}

UnsignedAssertion *
UnsignedAssertion_set(UnsignedAssertion * this, fixed PackageClass * package,
                      UnsignedLong expected, UnsignedLong actual) {
    UnsignedAssertion_set1(this, &UnsignedAssertion_class,
                           Core_pc(), Core_sp(), package,
                           expected, actual);
    return this;
}

void
UnsignedAssertion_throw(fixed ObjectClass * from, const UnsignedLong expected, const UnsignedLong actual) {
    UnsignedAssertion * a = (UnsignedAssertion *)Core_createException(sizeof(UnsignedAssertion));
    UnsignedAssertion_set1(a, &UnsignedAssertion_class,
                           Core_pc(), Core_sp(), Fixed_fixpointer(&from->package),
                           expected, actual);
    Core_throw((Exception *)a);
}

fixed InterfaceClass * fixed UnsignedAssertion_interfaces[] fixedattr = {
    (void *) &UnsignedAssertion_interfaces[UnsignedAssertion_Interfaces_SIZE]
    Interface_LIST(Exception_Sized, &UnsignedAssertion_Sized_class)
    Interface_LIST(Exception_Printable, &UnsignedAssertion_Printable_class)
};

fixed UnsignedAssertionClass UnsignedAssertion_class fixedattr = {
    {
        {
            {
                &Package_class,
                UnsignedAssertion_interfaces
            }
        }
    }
};
