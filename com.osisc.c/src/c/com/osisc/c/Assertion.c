/*
 *  Copyright (c) 2018 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_Assertion

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

Assertion *
Assertion_set1(Assertion * this, fixed AssertionClass * class,
               fixed void * pc, void * sp, fixed PackageClass * package) {
    Exception_set1(&this->base, &class->base, pc, sp, package);
    return this;
}

Assertion *
Assertion_set(Assertion * this, fixed PackageClass * package) {
    Assertion_set1(this, &Assertion_class, Core_pc(), Core_sp(), package);
    return this;
}

void Assertion_throw(fixed ObjectClass * from) {
    Assertion * a = (Assertion *)Core_createException(sizeof(Assertion));
    Assertion_set1(a, &Assertion_class, Core_pc(), Core_sp(), Fixed_fixpointer(&from->package));
    Core_throw((Exception *)a);
}

fixed struct AssertionClass Assertion_class fixedattr = {
    {
        {
            &Package_class,
            Exception_interfaces
        }
    }
};

