/*
 *  Copyright (c) 2018-2020 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_Core

#include <stdlib.h>

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Pool.h"
#include "com/osisc/c/Job.h"
#include "com/osisc/c/List.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/Log.h"
#include "com/osisc/c/StringFormat.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/target/Core.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Pool.h"
#include "com/osisc/c/Job.h"
#include "com/osisc/c/List.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/Log.h"
#include "com/osisc/c/StringFormat.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/OutputFile.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

#if weakpragma
#   pragma weak com_osisc_c_OutputFile_printf
#   pragma weak com_osisc_c_OutputFile_oprinth
#   pragma weak com_osisc_c_OutputFile_oprint
#   pragma weak com_osisc_c_Log_exception
#   pragma weak com_osisc_c_Log_save
#endif

typedef struct CorePrivates CorePrivates;

struct CorePrivates {
    Core public;
    UnsignedShort cycle;
    List jobs;
    unsigned exiting : 1;
};

static CorePrivates * cores; /* TODO: Change to list / array for multicore support */

static CorePrivates *
CorePrivates_set(CorePrivates * this, fixed CoreConfigurationClass * config,
                 OutputFile * out, Pool * pool, Log * log) {
    Object_set(&this->public.base, &Core_class.base);
    this->public.config = config;
    this->public.out = out;
    this->public.pool = pool;
    this->public.log = log;
    this->cycle = 0;
    List_set(&this->jobs);
    this->exiting = 0;
    {
        Exception * exception = Fixed_pointer(&config->exceptionBase);
        ((Object *)exception)->class = (void *)0;
    }
    cores = this;
    return this;
}

Core * Core_create(fixed CoreConfigurationClass * config,
                   OutputFile * out, Pool * pool, Log * log) {
    Size size = Fixed_size(&config->size);
    Size coreSize = Fixed_size(&Core_class.size);
    Size exceptionSize = Fixed_size(&config->exceptionSize);

    if (coreSize != Core_SIZE) {
        return (void *)0;
    }
    if (size < coreSize || exceptionSize < sizeof(UnsignedAssertion)) {
        return (void *)0;
    }
    return (Core *)CorePrivates_set(Fixed_pointer(&config->address), config, out, pool, log);
}

Core * Core_current(void) {
    return &cores->public;
}

void Core_addJob(Job * job) {
    CorePrivates * private = cores;

    Assertion_TRUE(1, job);

    /* FIXME Lock interrupts */
    job->info.state = JobState_WAITING;
    List_addLast(&private->jobs, &job->link);
}

Bool Core_cancelJob(Job * job) {
    /* FIXME Lock interrupts */
    if (job->info.state == JobState_WAITING) {
        job->info.state = JobState_CANCELLED;
        return Bool_TRUE;
    }
    return Bool_FALSE;
}

void Core_runJobs(void) {
    CorePrivates * private = cores;

    while (1) {
        /* FIXME Lock interrupts */
        ListLink * link = List_removeFirst(&private->jobs);
        Job * job = structof(Job, link, link);

        if (job) {
            fixed JobClass * jobClass = (fixed JobClass *)((Object *)job)->class;
            void (*done)(Job *, Exception *) = Fixed_pointer(&jobClass->done);

            if (job->info.state != JobState_CANCELLED) {
                void (*run)(Job *) = Fixed_pointer(&jobClass->run);

                private->cycle++;

                /* TODO Exception handling */
                job->info.state = JobState_RUNNING;
                run(job);
                job->info.state = JobState_DONE;
            }
            done(job, (void *)0);
            job = (void *)0;
        } else {
            return;
        }
    }
}

void Core_run(void) {
    CorePrivates * private = cores;

    while (1) {
        Core_runJobs();

        if (!private->exiting) {
            /* TODO Sleep */
        } else {
            return;
        }
    }
}

UnsignedShort Core_cycle(void) {
    CorePrivates * private = cores;
    return private->cycle;
}

void Core_exit(Int value) {
    if (!value) {
        /* TODO Multicore support: set all cores exiting */
        CorePrivates * private = cores;
        private->exiting = 1;
    } else {
        /* TODO Multicore support: shutdown all cores */
        exit(value);
    }
}

Exception * Core_createException(Size size) {
    CorePrivates * private = (CorePrivates *)Core_current();
    fixed CoreConfigurationClass * config = private->public.config;
    Size max = Fixed_size(&config->exceptionSize);
    Exception * exception = Fixed_pointer(&config->exceptionBase);

    Assertion_TEST(0, Unsigned, max, >=, size);

    ((Object *)exception)->class = (ObjectClass *)&Exception_class;
    return exception;
}

Exception * Core_pendingException(void) {
    CorePrivates * private = (CorePrivates *)Core_current();
    fixed CoreConfigurationClass * config = private->public.config;
    Exception * exception = Fixed_pointer(&config->exceptionBase);
    return (((Object *)exception)->class) ? exception : (void *)0;
}

void Core_throw(Exception * e) {
    CorePrivates * private = (CorePrivates *)Core_current();
    fixed CoreConfigurationClass * config = private->public.config;
    Exception * exception = Fixed_pointer(&config->exceptionBase);

    Assertion_TEST(0, Pointer, exception, ==, e);
    Assertion_TRUE(0, ((Object *)exception)->class);

    if (private->public.out) {
        OutputFile_printf(private->public.out, "\n");
        OutputFile_oprinth(private->public.out, (Object *)e);
        OutputFile_oprint(private->public.out, (Object *)e);
    }
    if (private->public.log) {
        Log_exception(private->public.log, e);
        Log_save(private->public.log);
    }
    Core_exit(-1);
}

fixed InterfaceClass * fixed Core_interfaces[] fixedattr = {
    (void *) &Core_interfaces[Core_Interfaces_SIZE]
    Interface_LIST(Core_Assertable, &Core_Assertable_class)
};

fixed struct CoreClass Core_class fixedattr = {
    {
        &Package_class,
        Core_interfaces
    },
    sizeof(CorePrivates) /* FIXME: Why not implement Sized? */
};
