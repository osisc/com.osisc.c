/*
 *  Copyright (c) 2018 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_Exception

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/Location.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/Location.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

Exception *
Exception_set(Exception * this, fixed PackageClass * package) {
    Exception_set1(this, &Exception_class, Core_pc(), Core_sp(), package);
    return this;
}

Exception *
Exception_set1(Exception * this, fixed ExceptionClass * class,
               fixed void * pc, void * sp, fixed PackageClass * package) {
    Object_set(&this->base, &class->base);
    Location_set(&this->pc, pc, package);
    this->sp = sp;
    return this;
}

fixed InterfaceClass * fixed Exception_interfaces[] fixedattr = {
    (void *) &Exception_interfaces[Exception_Interfaces_SIZE]
    Interface_LIST(Exception_Sized, &Exception_Sized_class)
    Interface_LIST(Exception_Printable, &Exception_Printable_class)
};

fixed struct ExceptionClass Exception_class fixedattr = {
    {
        &Package_class,
        Exception_interfaces
    }
};
