/*
 *  Copyright (c) 2018-2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_Object

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

Object * Object_set(Object * this, fixed ObjectClass * class) {
    Assertion_TRUE(1, this);
    Assertion_TRUE(1, class);
    this->class = class;
    return this;
}

static fixed InterfaceClass *
interfaceOf(fixed InterfaceClass * interface, fixed InterfaceClass * class) {
    fixed InterfaceClass * super = Fixed_fixpointer(&interface->super);
    return (super == class) ? interface : (void *)0;
}

fixed InterfaceClass *
Object_interfaceOf(const Object * this, UnsignedInt id, fixed InterfaceClass * class) {
    fixed InterfaceClass * fixed * list = Fixed_fixpointer(&this->class->interfaces);
    fixed InterfaceClass * fixed * end = Fixed_fixpointer(&list[0]);

    if (list) {
        if (id) {
            if (&list[id] < end) {
                fixed InterfaceClass * interface = Fixed_fixpointer(&list[id]);
                return interfaceOf(interface, class);
            }
        } else while (++list < end) {
            fixed InterfaceClass * interface = Fixed_fixpointer(list);
            if (interfaceOf(interface, class)) {
                return interface;
            }
        }
    }
    return (void *)0;
}

fixed struct ObjectClass Object_class fixedattr = {
    &Package_class,
    (void *)0
};
