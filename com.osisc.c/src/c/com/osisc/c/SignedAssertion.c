/*
 *  Copyright (c) 2018-2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_SignedAssertion

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

SignedAssertion *
SignedAssertion_set1(SignedAssertion * this, fixed SignedAssertionClass * class,
                     fixed void * pc, void * sp, fixed PackageClass * package,
                     const Long expected, const Long actual) {
    Assertion_set1(&this->base, &class->base, pc, sp, package);
    this->expected = expected;
    this->actual = actual;
    return this;
}

SignedAssertion *
SignedAssertion_set(SignedAssertion * this, fixed PackageClass * package, Long expected, Long actual) {
    SignedAssertion_set1(this, &SignedAssertion_class,
                         Core_pc(), Core_sp(), package,
                         expected, actual);
    return this;
}

void SignedAssertion_throw(fixed ObjectClass * from, const Long expected, const Long actual) {
    SignedAssertion * a = (SignedAssertion *)Core_createException(sizeof(SignedAssertion));
    SignedAssertion_set1(a, &SignedAssertion_class,
                         Core_pc(), Core_sp(), Fixed_fixpointer(&from->package),
                         expected, actual);
    Core_throw((Exception *)a);
}

fixed InterfaceClass * fixed SignedAssertion_interfaces[] fixedattr = {
    (void *) &SignedAssertion_interfaces[SignedAssertion_Interfaces_SIZE]
    Interface_LIST(Exception_Sized, &SignedAssertion_Sized_class)
    Interface_LIST(Exception_Printable, &SignedAssertion_Printable_class)
};

fixed struct SignedAssertionClass SignedAssertion_class fixedattr = {
    {
        {
            {
                &Package_class,
                SignedAssertion_interfaces
            }
        }
    }

};
