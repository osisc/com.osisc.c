/*
 *  Copyright (c) 2018-2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_PointerAssertion

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

PointerAssertion *
PointerAssertion_set1(PointerAssertion * this, fixed PointerAssertionClass * class,
                      fixed void * pc, void * sp, fixed PackageClass * package,
                      const void * expected, const void * actual) {
    Assertion_set1(&this->base, &class->base, pc, sp, package);
    this->expected = expected;
    this->actual = actual;
    return this;
}

PointerAssertion *
PointerAssertion_set(PointerAssertion * this, fixed PackageClass * package,
                     const void * expected, const void * actual) {
    PointerAssertion_set1(this, &PointerAssertion_class,
                          Core_pc(), Core_sp(), package,
                          expected, actual);
    return this;
}

void
PointerAssertion_throw(fixed ObjectClass * from, const void * expected, const void * actual) {
    PointerAssertion * a = (PointerAssertion *)Core_createException(sizeof(PointerAssertion));
    PointerAssertion_set1(a, &PointerAssertion_class,
                          Core_pc(), Core_sp(), Fixed_fixpointer(&from->package),
                          expected, actual);
    Core_throw((Exception *)a);
}

fixed InterfaceClass * fixed PointerAssertion_interfaces[] fixedattr = {
    (void *) &PointerAssertion_interfaces[PointerAssertion_Interfaces_SIZE]
    Interface_LIST(Exception_Sized, &PointerAssertion_Sized_class)
    Interface_LIST(Exception_Printable, &PointerAssertion_Printable_class)
};

fixed struct PointerAssertionClass PointerAssertion_class fixedattr = {
    {
        {
            {
                &Package_class,
                PointerAssertion_interfaces
            }
        }
    }
};
