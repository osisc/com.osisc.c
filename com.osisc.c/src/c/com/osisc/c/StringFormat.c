/*
 *  Copyright (c) 2018 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_StringFormat

#include <string.h>

#include "com/osisc/c/Object.h"
#include "com/osisc/c/StringFormat.h"
#include "com/osisc/c/target/Type.h"


#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/StringFormat.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

static int isDigit(char c) {
    return (c >= '0') && (c <= '9');
}

static fixed char * parseDigits(UnsignedShort * val, fixed char * s) {
    UnsignedShort sum = 0U;
    char c = Fixed_char(s);

    while (isDigit(c)) {
        sum *= 10U;
        sum += (UnsignedShort)(c - '0');
        c = Fixed_char(++s);
    }
    *val = sum;
    return s;
}

static fixed char * parseFlags(StringFormat * this, fixed char * s) {
    char c = Fixed_char(s);

    while (1) {
        switch (c) {
            case '!': this->skip = 1;
                break;
            case '0': this->zeroPad = 1;
                break;
            case '-': this->leftAlign = 1;
                break;
            case '+': this->plusSign = 1;
                break;
            case ' ': this->spaceSign = 1;
                break;
            case '#': this->altForm = 1;
                break;
            default:
                return s;
        }
        c = Fixed_char(++s);
    }
    return s;
}

static fixed char * parseWidth(StringFormat * this, fixed char * s) {
    char c = Fixed_char(s);

    if (isDigit(c)) {
        s = parseDigits(&this->width, s);
    } else if (c == '*') {
        this->widthArg = 1;
        s++;
    } else {}
    return s;
}

static fixed char * parsePrecision(StringFormat * this, fixed char * s) {
    char c = Fixed_char(s);

    if (c == '.') {
        c = Fixed_char(++s);

        if (isDigit(c)) {
            s = parseDigits(&this->precision, s);
        } else if (c == '*') {
            this->precisionArg = 1;
            s++;
        } else {}
    }
    return s;
}

static fixed char * parseModifiers(StringFormat * this, fixed char * s) {
    char c = Fixed_char(s);

    switch (c) {
        case 'l': {
            char c1 = Fixed_char(s+1);
            if (c1 == 'l') {
                this->type = TypeID_LONG_LONG;
                s++;
            } else {
                this->type = TypeID_LONG;
            }
            break;
        }
        case 'h': {
            char c1 = Fixed_char(s+1);
            if (c1 == 'h') {
                this->type = TypeID_CHAR;
                s++;
            } else {
                this->type = TypeID_SHORT;
            }
            break;
        }
        case 't': this->type = TypeID_PTRDIFF;
            break;
        case 'z': this->type = TypeID_SIZE;
            break;
        case 'L': this->type = TypeID_LONG_DOUBLE;
            break;
        default:
            return s;
    }
    return ++s;
}

static fixed char * parseSpecifier(StringFormat * this, fixed char * s) {
    char c = Fixed_char(s);

    switch (c) {
        case 'i':
            this->isSigned = 1;
            this->kind = IOKind_INT;
            break;
        case 'd': this->isSigned = 1;
        case 'u':
            this->kind = IOKind_INT;
            this->radix = 10U;
            break;
        case 'X': this->upperCase = 1;
        case 'x':
            this->kind = IOKind_INT;
            this->radix = 16U;
            break;
        case 'o':
            this->kind = IOKind_INT;
            this->radix = 8U;
            break;
        case 'b':
            this->kind = IOKind_INT;
            this->radix = 1U;
            break;
        case 'F': this->upperCase = 1;
        case 'f':
            this->kind = IOKind_FLOAT;
            this->fixForm = 1;
            this->radix = 10U;
            break;
        case 'E': this->upperCase = 1;
        case 'e':
            this->kind = IOKind_FLOAT;
            this->expForm = 1;
            this->radix = 10U;
            break;
        case 'G': this->upperCase = 1;
        case 'g':
            this->kind = IOKind_FLOAT;
            this->radix = 10U;
            break;
        case 'A': this->upperCase = 1;
        case 'a':
            this->kind = IOKind_FLOAT;
            this->radix = 16U;
            break;
        case 'c': this->kind = IOKind_CHAR;
            break;
        case 's': this->kind = IOKind_STRING;
            break;
        case 'p': this->kind = IOKind_POINTER;
            break;
        case 'n': this->kind = IOKind_NUM_CHARS;
            break;
        case '%': this->kind = IOKind_PERCENT;
            break;
        default:
            break;
    }

    switch ((IOKindValue)this->kind) {
        case IOKind_STRING:
            if (this->type == TypeID_LONG) {
                this->isWide = 1;
            }
            this->type = TypeID_POINTER;
            break;
        case IOKind_CHAR:
            this->type = (this->type == TypeID_LONG) ? TypeID_WCHAR : TypeID_CHAR;
            break;
        case IOKind_INT: this->type = (this->type) ? this->type : TypeID_INT;
            break;
        case IOKind_POINTER: this->type = TypeID_POINTER;
            break;
        case IOKind_FLOAT:
            switch (this->type) {
                case TypeID_LONG: this->type = TypeID_DOUBLE;
                    break;
                case TypeID_LONG_DOUBLE:
                    break;
                default: this->type = TypeID_FLOAT;
                    break;
            }
            break;
        case IOKind_NUM_CHARS: this->type = TypeID_POINTER;
            break;
        case IOKind_VERBATIM:
        case IOKind_PERCENT:
        case IOKind_VALUES:
        default: this->type = TypeID_VOID;
            break;
    }

    if (this->kind != IOKind_VERBATIM) {
        s++;
    }
    return s;
}

StringFormat * StringFormat_set(StringFormat * this, fixed char * s) {
    memset(this, 0, sizeof(*this));

    Object_set((Object *)this, (ObjectClass *)&StringFormat_class);

    char c = Fixed_char(s);

    if (c == '%') {
        s++;
        s = parseFlags(this, s);
        s = parseWidth(this, s);
        s = parsePrecision(this, s);
        s = parseModifiers(this, s);
        s = parseSpecifier(this, s);
    } else while (c != '%' && c != 0) {
        c = Fixed_char(++s);
    }

    this->next = s;
    return this;
}

fixed struct StringFormatClass StringFormat_class fixedattr = {
    {
        &Package_class,
        (void *)0
    }
};
