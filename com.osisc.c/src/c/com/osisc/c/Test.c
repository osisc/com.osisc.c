/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_Test

#include "com/osisc/c/Test.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Test.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

Test * Test_set(Test * this, fixed TestClass * class) {
    Object_set((Object *)this, (fixed ObjectClass *)class);
    return this;
}

fixed struct TestClass Test_class fixedattr = {
    {
        &Package_class,
        (void *)0
    },
    (void *)0
};

