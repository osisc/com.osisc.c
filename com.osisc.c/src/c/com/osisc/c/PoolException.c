/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_PoolException

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Pool.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Pool.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

PoolException *
PoolException_set(PoolException * this, fixed PackageClass * package, Pool * pool, Size size) {
    Exception_set1(&this->base, &PoolException_class.base, Core_pc(), Core_sp(), package);
    this->pool = pool;
    this->size = size;
    return this;
}

fixed InterfaceClass * fixed PoolException_interfaces[] fixedattr = {
    (void *) &PoolException_interfaces[PoolException_Interfaces_SIZE]
    Interface_LIST(Exception_Sized, &PoolException_Sized_class)
    Interface_LIST(Exception_Printable, &PoolException_Printable_class)
};

fixed struct PoolExceptionClass PoolException_class fixedattr = {
    {
        {
            &Package_class,
            PoolException_interfaces
        }
    }
};


