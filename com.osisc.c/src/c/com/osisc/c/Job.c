/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_Job

#include "com/osisc/c/Job.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Pool.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Job.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Pool.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

#ifdef weakpragma
#   pragma weak com_osisc_c_Pool_return
#endif

Job * Job_set(Job * this, fixed JobClass * class) {
    Object_set((Object *)this, (fixed ObjectClass *)class);
    this->link.next = (void *)0;
    this->info.state = JobState_CREATED;
    return this;
}

void Job_run(Job * this) {
    (void)this;
}

void Job_done(Job * this, Exception * e) {
    Pool * pool = Core_current()->pool;

    if (pool) {
        Pool_return(pool, this);
    }
    (void)e;
}

fixed struct JobClass Job_class fixedattr = {
    {
        &Package_class,
        (void *)0
    },
    Job_run,
    Job_done
};
