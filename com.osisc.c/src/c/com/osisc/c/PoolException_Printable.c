/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_PoolException

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Pool.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Pool.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

phantom char PoolException_headFormat[] phantomattr =
    EXCEPTION_H_FMT "\t" "Pool" "\t" "Size";
phantom char PoolException_dataFormat[] phantomattr =
    EXCEPTION_D_FMT "\t" "%p" "\t" "%" SIZE_U_FMT;

fixed PrintableClass PoolException_Printable_class fixedattr = {
    { &Printable_class },
    PoolException_headFormat,
    PoolException_dataFormat
};
