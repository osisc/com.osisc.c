/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_TestRunner

#include "com/osisc/c/Test.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Test.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

phantom char TestRunner_headFormat[] phantomattr =
    "Tests" "\t" "Passed";
phantom char TestRunner_dataFormat[] phantomattr =
    "%!p%!p" "%" SHORT_U_FMT "\t" "%" SHORT_U_FMT;

fixed PrintableClass TestRunner_Printable_class fixedattr = {
    { &Printable_class },
    TestRunner_headFormat,
    TestRunner_dataFormat
};
