/*
 *  Copyright (c) 2019-2020 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c_test
#define com_osisc_c_CLASS com_osisc_c_test_AssertionTest

#include "com/osisc/c/Test.h"
#include "com/osisc/c/Job.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/StringFormat.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/test/TestPackage.h"
#include "com/osisc/c/test/AssertionTest.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Test.h"
#include "com/osisc/c/Job.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/StringFormat.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/test/TestPackage.h"
#include "com/osisc/c/test/AssertionTest.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

AssertionTest * AssertionTest_set(AssertionTest * this) {
    Test_set((Test *)this, (TestClass *)&AssertionTest_class);
    return this;
}

static Exception * createAssertion(AssertionTest * this, Int i) {
    switch (i) {
        case 0: Exception_set(&this->area.exception,
                              &TestPackage_class.base);
            break;
        case 1: Assertion_set(&this->area.assertion,
                              &TestPackage_class.base);
            break;
        case 2: PointerAssertion_set(&this->area.pointerAssertion,
                                     &TestPackage_class.base, (void *)0, createAssertion);
            break;
        case 3: SignedAssertion_set(&this->area.signedAssertion,
                                    &TestPackage_class.base, 0, -1);
            break;
        case 4: UnsignedAssertion_set(&this->area.unsignedAssertion,
                                      &TestPackage_class.base, 0, 1);
            break;
    }
    return &this->area.exception;
}

static UnsignedInt numFields(fixed char *s) {
    UnsignedInt fields = 1;

    Assertion_TRUE(0, s);
    while((s = Fixed_strchr(s, '\t'))) {
        fields++;
        s++;
    }
    return fields;
}

void AssertionTest_testPrintable(Test * this) {
    PrintableClass * printable0;
    UnsignedInt id = CONDITION(Exception_Printable)
        (Exception_Printable_ID, 0);
    UnsignedInt fields0 = CONDITION(Exception_Printable)
        (0, sizeof(Exception) / sizeof(void *));
    Int i;

    for (i = 0; i < 5; i++) {
        Exception * a = createAssertion((AssertionTest *)this, i);

        PrintableClass * printable1 = (PrintableClass *)
            Object_interfaceOf((Object *)a, id, &Printable_class);
        PrintableClass * printable2 = (PrintableClass *)
            Object_interfaceOf((Object *)a, 0, &Printable_class);

        Assertion_TEST(0, Pointer, printable1, ==, printable2);

        if (i == 0) {
            printable0 = printable1;
        }
        if (printable1) {
            UnsignedInt headFields = numFields(Fixed_phantompointer(&printable1->headFormat));
            UnsignedInt dataFields = numFields(Fixed_phantompointer(&printable1->dataFormat));

            Assertion_TEST(0, Unsigned, headFields, ==, dataFields);

            if (i == 0) {
                fields0 = headFields;
            } else if (i > 1) {
                Assertion_TRUE(0, printable1 != printable0);
                Assertion_TEST(0, Unsigned, fields0 + 2U, ==, headFields);
            } else {}
        } else {
            Assertion_TRUE(0, !CONDITION(Exception_Printable) (1,0));
            Assertion_TRUE(0, !printable0);
        }
    }
}

void AssertionTest_testSized(Test * this) {
    SizedClass * sized0;
    UnsignedInt id = CONDITION(Exception_Sized) (Exception_Sized_ID, 0);
    Int i;

    for (i = 0; i < 5; i++) {
        Exception * a =
            createAssertion((AssertionTest *)this, i);
        SizedClass * sized1 = (SizedClass *)
            Object_interfaceOf((Object *)a, id, &Sized_class);
        SizedClass * sized2 = (SizedClass *)
            Object_interfaceOf((Object *)a, 0, &Sized_class);

        Assertion_TEST(0, Pointer, sized1, ==, sized2);

        if (i == 0) { 
        	sized0 = sized1;
       	}
        if (sized1) {
            Size size = Fixed_size(&sized1->size);
            
            if (i > 1) {
                Assertion_TRUE(0, sized1 != sized0);
                Assertion_TRUE(0, size > sizeof(Exception));
            }
        } else {
            Assertion_TRUE(0, !CONDITION(Exception_Sized)(1,0));
            Assertion_TRUE(0, !sized0);
        }
    }
}

static TestFunction * fixed AssertionTest_functions[] fixedattr = {
    AssertionTest_testPrintable,
    AssertionTest_testSized,
    (void *)0
};

fixed struct AssertionTestClass AssertionTest_class fixedattr = {
    {
        {
            &TestPackage_class.base,
            (void *)0
        },
        AssertionTest_functions
    }
};

