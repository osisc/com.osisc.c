/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_HelloJob

#include <string.h>

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Job.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/test/TestPackage.h"
#include "com/osisc/c/test/HelloTest.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Job.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/test/TestPackage.h"
#include "com/osisc/c/test/HelloTest.h"
#undef IMPORTS

HelloJob * HelloJob_set(HelloJob * this, OutputFile * out, char * s) {
    Job_set((Job *)this, (fixed JobClass *)&HelloJob_class);
    this->out = out;
    strcpy(this->s, s);
    return this;
}

void HelloJob_run(Job * this) {
    HelloJob * job = (HelloJob *)this;
    OutputFile_printf(job->out, job->s);
}

void HelloJob_done(Job * this, Exception * e) {
    HelloJob * job = (HelloJob *)this;

    if (strcmp(job->s, "Hello, ") == 0) {
        strcpy(job->s, "world!\n");
        Core_addJob(this);
        Core_exit(0);
    } else {
        Job_done(this, e);
    }
}

fixed struct HelloJobClass HelloJob_class fixedattr = {
    {
        {
            &TestPackage_class.base,
            (void *)0
        },
        HelloJob_run,
        HelloJob_done
    }
};

