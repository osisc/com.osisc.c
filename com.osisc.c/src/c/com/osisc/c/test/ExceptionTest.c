/*
 *  Copyright (c) 2019-2020 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c_test
#define com_osisc_c_CLASS com_osisc_c_test_ExceptionTest

#include "com/osisc/c/Test.h"
#include "com/osisc/c/Job.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/test/TestPackage.h"
#include "com/osisc/c/test/ExceptionTest.h"

#define IMPORTS
#include "com/osisc/c/Test.h"
#include "com/osisc/c/Job.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/test/TestPackage.h"
#include "com/osisc/c/test/ExceptionTest.h"
#undef IMPORTS

ExceptionTest * ExceptionTest_set(ExceptionTest * this) {
    Test_set((Test *)this, (fixed TestClass *)&ExceptionTest_class);
    return this;
}

void ExceptionTest_testCreate(Test * this) {
    Exception * e = Core_createException(sizeof(Exception));
    Exception_set(e, &TestPackage_class.base);
    {
        char var;
        Address spX = (Address)&var;
        Address sp0 = (Core_STACK_DIRECTION > 0) ? spX : spX - 11 * sizeof(Long);
        Address sp1 = (Core_STACK_DIRECTION > 0) ? spX + 11 * sizeof(Long) : spX;
        Address pc0 = (Address)ExceptionTest_testCreate;
        Address pc = (Address)e->pc.address;
        Address sp = (Address)e->sp;

        if (pc < pc0 || pc > (pc0 + 6 * sizeof(Long))) {
            Assertion_TEST(0, Pointer, (void *)pc0, ==, (void *)pc);
        }
        if (sp < sp0 || sp > sp1) {
            Assertion_TEST(0, Pointer, (void *)sp0, ==, (void *)sp);
        }
    }
    (void)this;
}

void ExceptionTest_testThrow(Test * this) {
    Exception * e = Core_pendingException();
    Assertion_TRUE(0, e);
    /* Core_throw(e); */
    (void)this;
}

static TestFunction * fixed ExceptionTest_functions[] fixedattr = {
    ExceptionTest_testCreate,
    ExceptionTest_testThrow,
    (void *)0
};

fixed struct ExceptionTestClass ExceptionTest_class fixedattr = {
    {
        {
            (PackageClass *)&TestPackage_class,
            (void *)0
        },
        ExceptionTest_functions
    }
};

