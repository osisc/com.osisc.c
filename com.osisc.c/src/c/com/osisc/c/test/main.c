/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c_test

#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/Test.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/test/HelloTest.h"
#include "com/osisc/c/test/ExceptionTest.h"
#include "com/osisc/c/test/AssertionTest.h"

#define IMPORTS
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/Assertion.h"
#include "com/osisc/c/Test.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/test/HelloTest.h"
#include "com/osisc/c/test/ExceptionTest.h"
#include "com/osisc/c/test/AssertionTest.h"
#undef IMPORTS

static Type coreArea[Core_SIZE / sizeof(Type) + 1];

/* The largest exception we know of: */
static Type exceptionsArea[sizeof(UnsignedAssertion) / sizeof(Type) + 1];

static fixed struct CoreConfigurationClass coreConfig fixedattr = {
    coreArea,
    sizeof(coreArea),
    exceptionsArea,
    sizeof(exceptionsArea)
};

static TestRunner runner;

static union Tests {
    HelloTest hello;
    ExceptionTest exception;
    AssertionTest assertion;
} tests;

int main(int argc, char **argv) {
    Core * core;
    OutputFile out;

    OutputFile_set(&out, (void *)0, "\002");
    core = Core_create(&coreConfig, &out, (void *)0, (void *)0);

    if (!core) {
        Core_exit(-1);
    }

    TestRunner_set(&runner, &out);
    TestRunner_run(&runner, (Test *)HelloTest_set(&tests.hello, &out));
    TestRunner_run(&runner, (Test *)ExceptionTest_set(&tests.exception));
    TestRunner_run(&runner, (Test *)AssertionTest_set(&tests.assertion));
    TestRunner_done(&runner);

    (void)argc;
    (void)argv;
    return 0;
}
