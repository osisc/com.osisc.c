/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c_test
#define com_osisc_c_CLASS com_osisc_c_test_HelloTest

#include "com/osisc/c/Test.h"
#include "com/osisc/c/Job.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/test/TestPackage.h"
#include "com/osisc/c/test/HelloTest.h"

#define IMPORTS
#include "com/osisc/c/Test.h"
#include "com/osisc/c/Job.h"
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/test/TestPackage.h"
#include "com/osisc/c/test/HelloTest.h"
#undef IMPORTS

HelloTest * HelloTest_set(HelloTest * this, OutputFile * out) {
    Test_set((Test *)this, (fixed TestClass *)&HelloTest_class);
    this->out = out;
    return this;
}

static Type helloArea[(sizeof(HelloJob) + sizeof("Hello, ")) / sizeof(Type) + 1];
static Type asyncArea[(sizeof(HelloJob) + sizeof("asynchronous ")) / sizeof(Type) + 1];

void HelloTest_testHello(Test * this) {
    HelloTest * test = (HelloTest *)this;
    HelloJob * helloJob = HelloJob_set((void *)helloArea, test->out, "Hello, ");
    HelloJob * asyncJob = HelloJob_set((void *)asyncArea, test->out, "asynchronous ");

    Core_addJob((Job *)helloJob);
    Core_addJob((Job *)asyncJob);
    (void)this;
}

static TestFunction * fixed HelloTest_functions[] fixedattr = {
    HelloTest_testHello,
    (void *)0
};

fixed struct HelloTestClass HelloTest_class fixedattr = {
    {
        {
            &TestPackage_class.base,
            (void *)0
        },
        HelloTest_functions
    }
};

