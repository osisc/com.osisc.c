/*
 *  Copyright (c) 2018 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c_test
#define com_osisc_c_CLASS com_osisc_c_test_ExecutableFile

#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

static fixed char name[] fixedattr = "com_osisc_c_test";

fixed struct ExecutableFileClass ExecutableFile_class fixedattr = {
    FILE_ID_KEY,
    FILE_ID_KEY,
    name
};
