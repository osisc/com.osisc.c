/*
 *  Copyright (c) 2018-2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c
#define com_osisc_c_CLASS com_osisc_c_Pool

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Pool.h"
#include "com/osisc/c/List.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Pool.h"
#include "com/osisc/c/List.h"
#include "com/osisc/c/Core.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/Exception.h"
#include "com/osisc/c/target/Type.h"

#include "com/osisc/c/preprocess.h"
#undef IMPORTS

#define Buffer_ENDMARK 0xEB

typedef struct BufferPrivates BufferPrivates;

struct BufferPrivates {
    ListLink link;
    Size size;
    Type public;
};

static BufferPrivates * Buffer_set(BufferPrivates * private, Size requestSize) {
    Byte * end = (Byte *)&private->public + requestSize;

    private->link.next = (void *)0;
    private->size = requestSize;
    *end = Buffer_ENDMARK;

    return private;
}

typedef struct PoolPrivates PoolPrivates;

struct PoolPrivates {
    Pool public;
    BufferPrivates * unused;
    List free[sizeof(UnsignedInt)];
    BufferPrivates buffers[1]; /* Must be last, actual pool memory */
};

Pool * Pool_create(fixed PoolConfigurationClass * config) {
    void * address = Fixed_pointer(&config->address);
    PoolPrivates * privates = (PoolPrivates *)address;

    Object_set(&privates->public.base, &Pool_class.base);
    privates->public.config = config;
    privates->unused = privates->buffers;

    return &privates->public;
}

static Size Pool_bufferSize(Size requestSize) {
    Size size = offsetof(BufferPrivates, public) + requestSize + 1;
    void * ptr = Address_align((void *)size, sizeof(Type));
    return (Size)ptr;
}

static Size Pool_unusedSize(PoolPrivates * private) {
    fixed PoolConfigurationClass * config = private->public.config;
    Type * address = Fixed_pointer(&config->address);
    Size usedSize = (Size)((Byte *)private->unused - (Byte *)address);
    Size poolSize = Fixed_size(&config->size);
    return poolSize - usedSize;
}

static BufferPrivates * Pool_createBuffer(PoolPrivates * private, Size requestSize, Size bufferSize) {
    BufferPrivates * buffer = private->unused;
    private->unused = (BufferPrivates *)((Byte *)private->unused + bufferSize);

    return Buffer_set(buffer, requestSize);
}

void * Pool_request(Pool * this, Size requestSize) {
    PoolPrivates * private = (PoolPrivates *)this;
    fixed PoolBufferSizes * sizes = &this->config->sizes;
    UnsignedInt list;
    PoolException * e;

    /* Find the free list that matches requested size */
    for (list = 0; list < lengthof(*sizes); list++) {
        Size listSize = Fixed_size(&(*sizes)[list]);

        if (listSize >= requestSize) {
            /* FIXME Lock interrupts */
            ListLink * link = List_removeFirst(&private->free[list]);
            BufferPrivates * buffer = structof(BufferPrivates, link, link);

            /* No free buffer in the list, check unused pool */
            if (!buffer) {
                Size bufferSize = Pool_bufferSize(listSize);

                /* Enough unused pool, create buffer and return */
                if (bufferSize <= Pool_unusedSize(private)) {
                    buffer = Pool_createBuffer(private, requestSize, bufferSize);
                    return &buffer->public;
                }
                e = (PoolException *)Core_createException(sizeof(PoolException));
                PoolException_set(e, &Package_class, this, requestSize);
                Core_throw((Exception *)e);
                return (void *)0;
            }
            return &buffer->public;
        }
    }
    e = (PoolException *)Core_createException(sizeof(PoolException));
    PoolException_set(e, &Package_class, this, requestSize);
    Core_throw((Exception *)e);
    return (void *)0;
}

void Pool_return(Pool * this, void * o) {
    PoolPrivates * private = (PoolPrivates *)this;
    void * low = (char *)private->buffers + offsetof(BufferPrivates, public);
    void * high = (char *)private->unused - offsetof(BufferPrivates, public);

    if (o > low && o < high) {
        fixed PoolBufferSizes * sizes = &this->config->sizes;
        BufferPrivates * buffer = structof(BufferPrivates, public, o);
        UnsignedInt list;
        PoolException * e;

        /* Find the list that matches buffer size */
        for (list = 0; list < lengthof(*sizes); list++) {
            Size listSize = Fixed_size(&(*sizes)[list]);

            if (listSize >= buffer->size) {
                /* FIXME Lock interrupts */
                List_addFirst(&private->free[list], &buffer->link);
                return;
            }
        }
        e = (PoolException *)Core_createException(sizeof(PoolException));
        PoolException_set(e, &Package_class, this, buffer->size);
        Core_throw((Exception *)e);
    }
}

fixed InterfaceClass * fixed Pool_interfaces[] fixedattr = {
    (void *) &Pool_interfaces[Pool_Interfaces_SIZE]
    Interface_LIST(Pool_Assertable, &Pool_Assertable_class.base)
};

fixed struct PoolClass Pool_class fixedattr = {
    {
        &Package_class,
        Pool_interfaces
    }
};
