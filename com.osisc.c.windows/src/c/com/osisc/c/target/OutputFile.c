/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c_target
#define com_osisc_c_CLASS com_osisc_c_target_OutputFile

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#include "com/osisc/c/Object.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/StringFormat.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/Type.h"
#include "com/osisc/c/StringFormat.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

static char * createFileName(fixed char * name, const char * suffix) {
    Size nameLen = Fixed_strlen(name);
    Size suffixLen = strlen(suffix);
    char * fileName = malloc(nameLen + suffixLen);

    if (fileName) {
        Fixed_mem(fileName, name, nameLen);
        memcpy(fileName + nameLen, suffix, suffixLen);
    }
    return fileName;
}

static void freeFileName(char * name) {
    free(name);
}

OutputFile *
OutputFile_set(OutputFile * this, fixed char * name, char * suffix) {
    this->file = (void *)0;

    if (!name) {
        if (*suffix == 1) {
            this->file = stdout;
        } else if (*suffix == 2) {
            this->file = stderr;
        } else {
            this->file = fdopen(*suffix, "w");
        }
    } else {
        char * fileName = createFileName(name, suffix);

        if (fileName) {
            this->file = fopen(fileName, "w");
            freeFileName(fileName);
        }
    }
    if (!this->file) {
        /* TODO: Throw exception? */
    }
    return this;
}

static char * copyFormatStr(StringFormat * format, fixed char * s) {
    Size n = (Size)(format->next - s);
    char * out = malloc(n + 1);

    if (!out) {
        /* TODO: Throw exception? */
    }
    memcpy(out, s, n);
    out[n] = 0;
    return out;
}

static void freeFormatStr(char * s) {
    free(s);
}

static void
printField(OutputFile * this, StringFormat * format, fixed char * s, const void * field) {
    FILE * file = this->file;

    char *fmt = copyFormatStr(format, s);

    switch ((TypeIDValue)format->type) {
        case TypeID_CHAR: fprintf(file, fmt, *(char *)field);
            break;
        case TypeID_SHORT: fprintf(file, fmt, *(short *)field);
            break;
        case TypeID_INT: fprintf(file, fmt, *(int *)field);
            break;
        case TypeID_LONG: fprintf(file, fmt, *(long *)field);
            break;
        case TypeID_LONG_LONG: fprintf(file, fmt, *(long long *)field);
            break;
        case TypeID_SIZE: fprintf(file, fmt, *(size_t *)field);
            break;
        case TypeID_PTRDIFF: fprintf(file, fmt, *(ptrdiff_t *)field);
            break;
        case TypeID_WCHAR: fprintf(file, fmt, *(wchar_t *)field);
            break;
        case TypeID_POINTER: fprintf(file, fmt, *(void **)field);
            break;
        case TypeID_FLOAT: fprintf(file, fmt, *(float *)field);
            break;
        case TypeID_DOUBLE: fprintf(file, fmt, *(double *)field);
            break;
        case TypeID_LONG_DOUBLE: fprintf(file, fmt, *(long double *)field);
            break;
        case TypeID_VOID:
        case TypeID_VALUES:
        default: fputs(fmt, file);
            break;
    }
    freeFormatStr(fmt);
}

void OutputFile_oprintf(OutputFile * this, fixed char * s, Object * o) {
    char * field = (char *)o;
    StringFormat format;

    while (1) {
        StringFormat_set(&format, s);

        if (format.next == s) {
            break;
        } else switch (format.kind) {
            case IOKind_NUM_CHARS:
                break;
            case IOKind_PERCENT: fputc('%', this->file);
                break;
            default:
                field = Address_align(field, Types_align(format.type));
                if (!format.skip) {
                    printField(this, &format, s, field);
                }
                field += Types_size(format.type);
                break;
        }
        s = format.next;
    }
}

void OutputFile_oprintx(OutputFile * this, Size size, Object * o) {
    Byte * p = (Byte *)(o + 1);
    Byte * end = (Byte *)o + size;

    fprintf(this->file, "$%p" "\t", o->class);

    while (p < end) {
        fprintf(this->file, "%02x ", *p++);
    }
    fputc('\n', this->file);
}

void OutputFile_oprintp(OutputFile * this, Object * o) {
    fprintf(this->file, "$%p" "\t" "%p", o->class, o);
    fputc('\n', this->file);
}

void OutputFile_oprinth(OutputFile * this, Object * o) {
    fixed InterfaceClass * ic;

    if ((ic = Object_interfaceOf(o, 0, &Printable_class))) {
        fprintf(this->file, Fixed_phantompointer(&((PrintableClass *)ic)->headFormat));
    } else if ((ic = Object_interfaceOf(o, 0, &Sized_class))) {
        fprintf(this->file, "Class" "\t" "Contents");
    } else {
        fprintf(this->file, "Class" "\t" "Address");
    }
    fputc('\n', this->file);
}

void OutputFile_oprint(OutputFile * this, Object * o) {
    fixed InterfaceClass * ic;

    if ((ic = Object_interfaceOf(o, 0, &Printable_class))) {
        OutputFile_oprintf(this, Fixed_phantompointer(&((PrintableClass *)ic)->dataFormat), o);
    } else if ((ic = Object_interfaceOf(o, 0, &Sized_class))) {
        OutputFile_oprintx(this, Fixed_size(&((SizedClass *)ic)->size), o);
    } else {
	    OutputFile_oprintp(this, o);
    }
    fputc('\n', this->file);
}

void OutputFile_printf(OutputFile * this, fixed char * s, ...) {
	va_list args;
	
	va_start(args, s);
	vfprintf(this->file, s, args);
	va_end(args);
}
