/*
 *  Copyright (c) 2020 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c_target
#define com_osisc_c_CLASS com_osisc_c_target_WindowsPackage

#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/WindowsPackage.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/WindowsPackage.h"
#undef IMPORTS

fixed struct WindowsPackageClass WindowsPackage_class fixedattr = {
    {
        0,
        0,
        &ExecutableFile_class
    }
};
