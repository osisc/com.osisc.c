/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c_Exception_Printable 0
#define com_osisc_c_Exception_Sized 1

#define com_osisc_c_TestRunner_Printable 0
#define com_osisc_c_TestRunner_Sized 1
