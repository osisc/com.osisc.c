/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c_target

#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/Location.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/Type.h"
#include "com/osisc/c/target/Location.h"
#undef IMPORTS

Location *
Location_set(Location * this, fixed void * address, fixed PackageClass * package) {
    this->address = address;
    (void)package;
    return this;
}

Location *
Location_set0(Location * this, fixed ObjectClass * address) {
    this->address = address;
    return this;
}
