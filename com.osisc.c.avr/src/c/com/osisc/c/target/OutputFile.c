/*
 *  Copyright (c) 2019 Mattias Bertilsson
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  Contributors:
 *      Mattias Bertilsson - Initial API and implementation
 *
 *  SPDX-License-Identifier: EPL-2.0
 */

#define com_osisc_c_target
#define com_osisc_c_CLASS com_osisc_c_target_OutputFile

#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/target/Type.h"

#define IMPORTS
#include "com/osisc/c/Object.h"
#include "com/osisc/c/target/OutputFile.h"
#include "com/osisc/c/target/Type.h"
#undef IMPORTS

OutputFile *
OutputFile_set(OutputFile * this, fixed char * name, char * suffix) {
    this->file = (void *)0;
    (void)name;
    (void)suffix;
    return this;
}

void OutputFile_oprintf(OutputFile * this, fixed char * s, Object * o) {
    (void)this;
    (void)s;
    (void)o;
}

void OutputFile_oprintx(OutputFile * this, Size size, Object * o) {
    (void)this;
    (void)size;
    (void)o;
}

void OutputFile_oprintp(OutputFile * this, Object * o) {
    (void)this;
    (void)o;
}

void OutputFile_oprinth(OutputFile * this, Object * o) {
    (void)this;
    (void)o;
}

void OutputFile_oprint(OutputFile * this, Object * o) {
    (void)this;
    (void)o;
}

void OutputFile_printf(OutputFile * this, fixed char * s, ...) {
    (void)this;
    (void)s;
}
